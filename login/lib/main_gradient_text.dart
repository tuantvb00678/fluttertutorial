import 'package:flutter/material.dart';
import 'package:login/app_theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: LinearGradientApp(),
    );
  }
}

class LinearGradientApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Flutter LinearGradient Tutorial by Woolha.com'),
        ),
        body: Center(
            child: Container(
              child: Center(
                child: Text(
                  'LinearGradient Example',
                  style: TextStyle(
                    fontSize: 36.0,
                    fontWeight: FontWeight.bold,
                    foreground: Paint()..shader = linearGradient,
                  ),
                ),
              ),
            )
        )
    );
  }

  //
  final Shader linearGradient = AppTheme.linearGradient.createShader(
    Rect.fromLTWH(0.0, 0.0, 500.0, 100.0),
  );
}