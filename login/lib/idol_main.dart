import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:login/app_theme.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// AnimationControllers can be created with `vsync: this` because of TickerProviderStateMixin.
class _MyStatefulWidgetState extends State<MyStatefulWidget> with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 5, // box shadow
          bottomOpacity: 0.2,
          leadingWidth: 200,
          leading: Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
            /** FlutterLogo Widget **/
            child: Image.asset("assets/idol/Logo.png"),
          ),
          //placing inbuilt icon on leading of appbar
          actions: <Widget>[
            IconButton(
              onPressed: (){},
              icon: Image.asset('assets/idol/Notification.png'),
              //using font awesome icon in action list of appbar
            ),
            IconButton(
              onPressed: (){},
              icon: Image.asset('assets/idol/Cart.png'),
              //cart+ icon from FontAwesome
            ),
            IconButton(
              onPressed: (){},
              icon: Image.asset('assets/idol/Guest.png'),
              //cart+ icon from FontAwesome
            ),
          ],
        ), //set app bar

        body: Container(
            height:300, //height of container
            alignment: Alignment.center, //alignment of content
            padding: EdgeInsets.all(20), //padding of container wrapper
            child: Image.asset("assets/idol/Logo.png"),
        ),
        bottomNavigationBar: Container(
            height: 60,
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(80)),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    AppTheme.pink.withOpacity(0.5),
                    AppTheme.blueSky.withOpacity(0.5),
                  ],
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: Offset(0, 10), // changes position of shadow
                  ),
                ],
            ),
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                    IconButton(
                      icon: Image.asset('assets/idol/menu-bottom-search.png'),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Image.asset('assets/idol/menu-bottom-rank.png'),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Image.asset('assets/idol/menu-bottom-guest.png'),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Image.asset('assets/idol/menu-bottom-hum.png'),
                      onPressed: () {},
                    ),
                ],
            ),
        )
    );
  }
}