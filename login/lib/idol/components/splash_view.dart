import 'package:flutter/material.dart';
import 'package:login/app_theme.dart';

class SplashView extends StatefulWidget {
  final AnimationController animationController;

  const SplashView({Key? key, required this.animationController})
      : super(key: key);




  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  Widget build(BuildContext context) {

    final _introductionAnimation = Tween<Offset>(begin: const Offset(0, 0), end: const Offset(0.0, -1.0)).animate(CurvedAnimation(
      parent: widget.animationController,
      curve: const Interval(0.0, 0.2, curve: Curves.fastOutSlowIn),
    ));
    
    return SlideTransition(
      position: _introductionAnimation,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 500,
              child: Image.asset('assets/idol/splash_view.png', fit: BoxFit.cover),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 20, bottom: 10),
              child: Text("Clearhead", style: AppTheme.heading),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 64, right: 64),
              child: Text("Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore", textAlign: TextAlign.center, style: AppTheme.bodyText),
            ),
            const SizedBox(height: 30,),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom + 16),
              child: InkWell(
                onTap: () {
                  widget.animationController.animateTo(0.2);
                },
                child: Container(
                  height: 58,
                  padding: const EdgeInsets.only(
                    left: 56.0,
                    right: 56.0,
                    top: 16,
                    bottom: 16,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(38.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 0,
                        blurRadius: 10,
                        offset: const Offset(0, 10), // changes position of shadow
                      ),
                    ],
                    gradient: LinearGradient(
                      colors: [
                        AppTheme.blueSky.withOpacity(1),
                        AppTheme.pink.withOpacity(1),
                      ],
                    ),
                  ),
                  child: const Text("Let's begin", style: AppTheme.buttonLabel,),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
