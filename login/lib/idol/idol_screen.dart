import 'package:login/fitness_app/fitness_app_home_screen.dart';
import 'package:login/idol/components/care_view.dart';
import 'package:login/idol/components/center_next_button.dart';
import 'package:login/idol/components/mood_diary_vew.dart';
import 'package:login/idol/components/relax_view.dart';
import 'package:login/idol/components/splash_view.dart';
import 'package:login/idol/components/top_back_skip_view.dart';
import 'package:login/idol/components/welcome_view.dart';
import 'package:flutter/material.dart';
import 'package:login/app_theme.dart';
import 'dart:math' as math;

class IdolScreen extends StatefulWidget {
  const IdolScreen({Key? key}) : super(key: key);

  @override
  _IdolScreenState createState() =>
      _IdolScreenState();
}

class _IdolScreenState
    extends State<IdolScreen> with TickerProviderStateMixin {
  AnimationController? _animationController;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: const Duration(seconds: 8));
    _animationController?.animateTo(0.0);
    super.initState();
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(_animationController?.value);
    return Scaffold(
      backgroundColor: AppTheme.white,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerRight,
            end: Alignment.bottomRight,
            colors: [
              AppTheme.pink.withOpacity(0.8),
              AppTheme.blueSky.withOpacity(0.8),
            ],
            //transform: GradientRotation(math.pi / 45), // 45deg
            transform: GradientRotation(math.pi / 45), // 180deg
          ),
        ),
        child: ClipRect(
          child: Stack(
            children: [
              SplashView(
                animationController: _animationController!,
              ),
              RelaxView(
                animationController: _animationController!,
              ),
              CareView(
                animationController: _animationController!,
              ),
              MoodDiaryVew(
                animationController: _animationController!,
              ),
              WelcomeView(
                animationController: _animationController!,
              ),
              TopBackSkipView(
                onBackClick: _onBackClick,
                onSkipClick: _onSkipClick,
                animationController: _animationController!,
              ),
              CenterNextButton(
                animationController: _animationController!,
                onNextClick: _onNextClick,
              ),
            ],
          ),
        ),
      )
    );
  }

  void _onSkipClick() {
    _animationController?.animateTo(0.8, duration: Duration(milliseconds: 1200));
  }

  void _onBackClick() {
    if (_animationController!.value >= 0 && _animationController!.value <= 0.2) {
      _animationController?.animateTo(0.0);
    } else if (_animationController!.value > 0.2 && _animationController!.value <= 0.4) {
      _animationController?.animateTo(0.2);
    } else if (_animationController!.value > 0.4 && _animationController!.value <= 0.6) {
      _animationController?.animateTo(0.4);
    } else if (_animationController!.value > 0.6 && _animationController!.value <= 0.8) {
      _animationController?.animateTo(0.6);
    } else if (_animationController!.value > 0.8 && _animationController!.value <= 1.0) {
      _animationController?.animateTo(0.8);
    }
  }

  void _onNextClick() {
    if (_animationController!.value >= 0 && _animationController!.value <= 0.2) {
      _animationController?.animateTo(0.4);
    } else if (_animationController!.value > 0.2 && _animationController!.value <= 0.4) {
      _animationController?.animateTo(0.6);
    } else if (_animationController!.value > 0.4 && _animationController!.value <= 0.6) {
      _animationController?.animateTo(0.8);
    } else if (_animationController!.value > 0.6 && _animationController!.value <= 0.8) {
      _signUpClick();
    }
  }

  void _signUpClick() {
    Navigator.pop(
        context,
        MaterialPageRoute(builder: (context) => FitnessAppHomeScreen())
    );
  }
}
